﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TestLambdaOptimization
{
    class LambdaOptimizer
    {
        /// <summary>
        /// Optimize expression tree by extracting and replacing all calls to
        /// same function with params (functionF) to one param. For each same
        /// call make new lambda in HashSet with specific name (p1,p2,...,pN)
        /// 
        /// Example:
        ///   (int x, int y) => F(x)>F(y)?F(x):(F(x)<F(2*y)?F(2*y):F(y))
        ///   
        ///   will be transpiled to hash set with three lambdas
        ///     p1 = (int x) => F(x)
        ///     p2 = (int y) => F(y)
        ///     p3 = (int y) => F(2*y)
        ///     
        ///   and return optimized expression as 
        ///   (p1, p2, p3) => p1 > p2?p1:(p1<p3?p3:p2);
        /// </summary>
        /// <param name="expression">to optimize</param>
        /// <param name="funcF">function calls to extract</param>
        /// <param name="paramsList">optimized params (calls to f(x) e.t.c) in order of optimization </param>
        public LambdaExpression Optimize<T,TResult>( 
            Expression expression, 
            Func<T,TResult> funcF, 
            out List<LambdaExpression> paramsList )
        {
            var visitor = new OptimizerVisitor<T,TResult>(funcF);
            var optimized = visitor.Optimize(expression);

            paramsList = visitor.NewParamsList;
            return Expression.Lambda(optimized, visitor.Params);

        }

        private class OptimizerVisitor<T,TResult> : ExpressionVisitor
        {
            private Func<T,TResult> functionF; //to find and replace
            private List<ParameterExpression> paramsList;
            private List<LambdaExpression> paramExecs; //executable params lambda in order of parmsList
            private Dictionary<string, ParameterExpression> paramsDict; //for faster search in paramsList

            private Dictionary<string, string> callToParmDict; // "f(x)" => "p1"
            private Dictionary<string, string> parmToCallDict; // "p1" => "f(x)"
            private Dictionary<string, Expression> callsDict;  // "f(x)" => F(x)
            private int currParm; //current found parameter pi

            public Dictionary<string, LambdaExpression> NewParamsDict { get; private set; }

            public List<LambdaExpression> NewParamsList { get; private set; }

            public ParameterExpression[] Params {
                get {
                    return paramsList.ToArray();
                }
            }

            public OptimizerVisitor( Func<T, TResult> funcF )
            {
                this.functionF = funcF;
                NewParamsDict = new Dictionary<string, LambdaExpression>();
                NewParamsList = new List<LambdaExpression>();
                paramsList = new List<ParameterExpression>();
                callToParmDict = new Dictionary<string, string>();
                parmToCallDict = new Dictionary<string, string>();
                callsDict = new Dictionary<string, Expression>();
                paramsDict = new Dictionary<string, ParameterExpression>();
                currParm = 1;
            }

            public Expression Optimize(Expression expression)
            {
                return Visit(expression);
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                if( node.Method.Name == functionF.Method.Name)
                {
                    var callStr = node.ToString();
                    if (!callsDict.ContainsKey(callStr))
                    {
                        callsDict.Add(callStr, node);
                        var parmName = $"p{currParm}";
                        currParm += 1;

                        callToParmDict.Add(callStr, parmName);
                        parmToCallDict.Add(parmName, callStr);

                        var newParm = Expression.Parameter(node.Type, parmName);

                        var lambda = Expression.Lambda(node, newParm);

                        NewParamsDict.Add(parmName, lambda);
                        NewParamsList.Add(lambda);
                        paramsList.Add(newParm);

                        return VisitParameter(newParm);
                    }
                    else
                    {
                        var parmName = callToParmDict[callStr];
                        var parm = paramsDict[parmName];
                        return VisitParameter(parm);
                    }
                }
                else
                {
                    return base.VisitMethodCall(node);
                }
            }
        }
    }
}
