﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestLambdaOptimization
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonOptimize_Click(object sender, EventArgs e)
        {
            Expression<Func<int,int,int>> lambda = 
                (int x, int y) => F(x) > F(y) ? F(x) : (F(x) < F(2 * y) ? F(2 * y) : F(y));

            List<LambdaExpression> parmsCalls;
            int[] parms = new int[2]{ 1, 2 } ;


            var optimized = OptimizeLamda<int,int>(lambda, F, out parmsCalls );
            var result = CalculateLambda<int,int>(optimized, parmsCalls, parms);

            labelResult.Text = $"Result:{result}";
        }

        private TResult CalculateLambda<T,TResult>(LambdaExpression optimized, List<LambdaExpression> parmsCalls, T[] parms)
        {
            List<T> calculatedParms = new List<T>();
            for (int i = 0; i < parmsCalls.Count; i++)
            {
                var call = parmsCalls[i];
                var value = parms[i];
                calculatedParms[i] = (T)call.Compile().DynamicInvoke(value);
            }

            return (TResult)optimized.Compile().DynamicInvoke(calculatedParms.ToArray());
        }

        private int F(int x)
        {
            return x = x*x;
        }

        private LambdaExpression OptimizeLamda<T,TResult>(Expression lambda, Func<T,TResult> funcF, out List<LambdaExpression> paramsList)
        {
            var optimizer = new LambdaOptimizer();

            paramsList = new List<LambdaExpression>();

            var optimizedLambda = optimizer.Optimize(
                lambda,
                funcF,
                out paramsList
                );

            return optimizedLambda;
        }
    }
}
