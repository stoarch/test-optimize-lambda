# Test for generating optimized lambda #

Task: Given lambda (int x, int y) => (f(x) + (f(x) < 5)?f(2*y):f(y))

Function f(x) is slow.

## Goal:## 
Need to make program that optimizes lambda (extracts all calls to f(x) and substitute it with cached values)
and return optimized lambda and list of params to calculate before running optimized version

e.g. (p1, p2, p3) => (p1 + (p1 < 5)?p2:p3)

where p1 => f(x), 
      p2 => f(2*y),
	  p3 => f(y)